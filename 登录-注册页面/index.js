let express = require("express");
let app = express();
app.use(express.urlencoded({ extended: true }));

//handlebars，连接handlebars
let handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({ defaultLayout: false }));
app.set("view engine", 'handlebars');
//sqlite3，连接数据库
let sqlite3 = require("sqlite3").verbose();
let db = new sqlite3.Database("./db.sqlite3");

//用户登录和注册,设置是否为合法用户
let session = require("express-session");
app.use(
    session({
        secret: "some secret code",
        resave: false,
        saveUninitialized: false
    })
);

//内部主页
app.get("/logined", function(req, res){
    res.send("登录成功")
});

//主页--登录页，注册页
app.get("/", function (req, res) {
    res.render('login', {
    })
});
app.get("/sign", function (req, res) {
    res.render('signup', {
    })
});

//用户注册(更多细节：如两次密码不相同和注册名不规范--添加提示(在网页前端进行提醒--jQuery))
app.post("/signup", function (req, res) {
    let username = req.body.username;
    let password = req.body.password1;
    let password2 = req.body.password2;
    console.log(username);
    console.log(password);
    console.log(password2);
    if (username.length <= 6 && password.length >= 3 && password === password2) {
        //先把用户注册名(长度3-6个字符）写进数据库,直接放在logsign表中
        db.run("INSERT INTO logsign(username, password) VALUES(?,?)", [username, password], function (err, rows) {
            console.log(err);
            res.redirect("/"); //注册成功，返回登录页
        });
    } else {
        res.redirect("back")//不成功，留在注册页
    };
});

//验证用户名是否存在
app.get("/checkUser", function (req, res) {
    let username = req.query.username;
    console.log(username);
    db.get("SELECT * FROM logsign WHERE username = ?", username, function (err, user) {
      if (user) {
        res.json({ "available": false });
      } else {
        res.json({ "available": true });
      }
    });
  });

//用户登录
app.post("/login", function (req, res) {
    let username = req.body.username;
    let password = req.body.password;
    db.get("SELECT * FROM logsign WHERE username=? AND password=?", [username, password], function (err, row) {
        if (row) {
            console.log(err);
            req.session['username'] = username;
            res.redirect("/logined");//登录成功，进入内部主页
        } else {
            res.redirect("back"); //不成功，留在登录页
        };
    });
});

app.listen(8000, function () {
    console.log("listening on port 8000")
});