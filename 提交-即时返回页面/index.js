let express = require('express');
let app = express();
let sqlite3 = require('sqlite3').verbose();
let db = new sqlite3.Database('./db.sqlite3');
let handlebars = require('express-handlebars');

app.engine("handlebars", handlebars({ defaultLayout: false }));
app.set('view engine', 'handlebars');
app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));

app.get("/", function (req, res) {
    db.all("SELECT id, title, content, DATETIME(time_added, 'localtime') as time_added FROM space", [], function (err, rows) {
        console.log(err);
        res.render("index", {
            "posts": rows,
        });
        console.log(rows);
    });
});

app.post("/submit", function (req, res) {
    console.log(req.body)
    let title = req.body.title;
    let content = req.body.content;

    db.run('INSERT INTO space(title,content) VALUES(?,?)', [title, content], function (err) {
        console.log(err);
        res.redirect("back");
    });
});

app.get("/delete/:id", function (req, res) {
    let post_id = req.params['id'];
    console.log(post_id);
    db.get("SELECT * FROM space WHERE id=?", [post_id], function (err, row) {
        db.run("DELETE FROM space WHERE id=?", [post_id], function (err) {
            console.log(err);
            res.redirect("back");
        });
    })
});


app.listen(8000, function () {
    console.log("listen on port 8000")
});